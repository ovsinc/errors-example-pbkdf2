package main

import (
	"context"
	"errors-example-pbkdf2/internal/services/common"
	"errors-example-pbkdf2/internal/services/pbkdf2/app"
	pbkdf2port "errors-example-pbkdf2/internal/services/pbkdf2/port"
	staticport "errors-example-pbkdf2/internal/services/static/port"
	"fmt"
	"net/http"

	fiber "github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/middleware/logger"
	"github.com/gofiber/template/html"
	"github.com/sirupsen/logrus"
	"go.uber.org/fx"

	errlogger "gitlab.com/ovsinc/errors/log"
	errlogrus "gitlab.com/ovsinc/errors/log/logrus"
)

func setLogger() *logrus.Logger {
	l := logrus.New()
	errlogger.DefaultLogger = errlogrus.New(l)
	return l
}

func htmlEngine() fiber.Views {
	return html.New(params.TemplatePath, ".html")
}

func staticHandler() staticport.StaticRenderer {
	return staticport.NewStaticRender()
}

func registryStatic(
	lifecycle fx.Lifecycle,
	app *fiber.App,
	statics staticport.StaticRenderer,
) error {
	lifecycle.Append(
		fx.Hook{
			OnStart: func(context.Context) error {
				staticport.RegistryStaticRenderService(
					app,
					common.ServicesMap{
						common.ServiceMap{
							Method:  http.MethodGet,
							Path:    indexPath,
							Handler: statics.Index,
						},
					},
				)
				return nil
			},
		},
	)
	return nil
}

func registryApi(
	lifecycle fx.Lifecycle,
	app *fiber.App,
	logger *logrus.Logger,
	kdfer app.Pbkdf2er,
) error {
	lifecycle.Append(
		fx.Hook{
			OnStart: func(context.Context) error {
				g := app.Group(apiPathGroup)
				api := pbkdf2port.NewKeyRender(logger, kdfer)

				pbkdf2port.RegistryKeyRenderService(
					g,
					common.ServicesMap{
						common.ServiceMap{
							Method:  http.MethodPost,
							Path:    pbkdf2Path,
							Handler: api.Key,
						},
					},
				)
				return nil
			},
		},
	)
	return nil
}

func httpServer(
	engine fiber.Views,
) *fiber.App {
	app := fiber.New(fiber.Config{
		Views:         engine,
		Prefork:       false,
		CaseSensitive: true,
		StrictRouting: true,
		ServerHeader:  "Fiber",
	})

	app.Use(logger.New())

	return app
}

func startService(
	lifecycle fx.Lifecycle,
	logger *logrus.Logger,
	app *fiber.App,
) error {
	lifecycle.Append(
		fx.Hook{
			OnStart: func(context.Context) error {
				logger.Infof("Static http server listen :%d", params.Port)
				go func() { _ = app.Listen(fmt.Sprintf(":%d", params.Port)) }()
				return nil
			},

			OnStop: func(context.Context) error {
				return app.Shutdown()
			},
		})

	return nil
}
