package main

import (
	"os"

	"github.com/segmentio/conf"
)

const (
	myprefix = "PKKDF2"
	version  = "1.0"
	appName  = "errors-example-pbkdf2"

	indexPath    = "/"
	apiPathGroup = "/api"
	pbkdf2Path   = "/pbkdf2"
)

type paramtype struct {
	Version      string `conf:"version" help:"Print application version and exit"`
	Port         int    `conf:"port" help:"Set tcp port for listening"`
	TemplatePath string `conf:"template" help:"Set template dir"`
}

var params paramtype

func init() {
	loader := conf.Loader{
		Name: appName,
		Args: os.Args[1:],
		Sources: []conf.Source{
			conf.NewEnvSource(myprefix, os.Environ()...),
		},
	}

	params = paramtype{
		Version:      version,
		Port:         8000,
		TemplatePath: "./assets/template",
	}

	conf.LoadWith(&params, loader)
}
