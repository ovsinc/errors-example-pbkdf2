package main

import (
	"errors-example-pbkdf2/internal/services/pbkdf2/adaptor"
	"errors-example-pbkdf2/internal/services/pbkdf2/app"
	"fmt"
	"time"

	"go.uber.org/fx"
)

const (
	GracefulStopTimeout = 10 * time.Second
	StartTimeout        = 10 * time.Second
)

func main() {
	appCtx := fx.New(
		// options
		fx.StartTimeout(StartTimeout),
		fx.StopTimeout(GracefulStopTimeout),

		fx.Provide(
			// common
			setLogger,
			httpServer,
			htmlEngine,

			// static
			staticHandler,

			// api
			app.NewGetKeyQuery,
			adaptor.Newkdf,
		),

		// static
		fx.Invoke(
			registryStatic,
			registryApi,
			startService,
		),
	)

	appCtx.Run()

	if err := appCtx.Err(); err != nil {
		fmt.Println(err)
	}
}
