BUILD = `date +%FT%T%z`
# APP_VERSION := `git describe --tags --abbrev=4`

_CURDIR := `git rev-parse --show-toplevel 2>/dev/null | sed -e 's/(//'`

_DOCKER_IMG := "test/test-errors-pbkdf2:latest"

# PKG := "gitlab.com/ovsinc/memory-rate-limits"
PKG_LIST := $(shell go list ${_CURDIR}/... 2>/dev/null)


linter := golangci-lint

mockery := mockery

test_run := go test -short -run=^Example


.PHONY: all
all: lint test build

.PHONY: lint
lint: go_lint go_security ## Full liner checks

.PHONY: go_security
go_security: ## Check bugs
	@${linter} run --disable-all \
	-E gosec -E govet -E exportloopref -E staticcheck -E typecheck

.PHONY: go_lint
go_lint: ## Lint the files
	@${linter} run

.PHONY: go_lint_max
go_lint_max: ## Max lint checks the files
	@${linter} run \
	-p bugs -p complexity -p unused -p format \
	-E gosec -E govet -E exportloopref -E staticcheck -E typecheck

.PHONY: go_style
go_style: ## check style of code
	@${linter} run -p style

.PHONY: test
test: unit_test race msan ## Run unittests

.PHONY: unit_test
unit_test: ## Run unittests
	@${test_run} ${PKG_LIST}

.PHONY: race
race: ## Run data race detector
	@${test_run} -race ${PKG_LIST}

.PHONY: msan
msan: ## Run memory sanitizer
	@CXX=clang++ CC=clang \
	${test_run} -msan ${PKG_LIST}

.PHONY: bench
bench: ## Run benchmark tests
	@${test_run} -benchmem -run=^# -bench=. ${_CURDIR}


.PHONY: coverage
coverage: ## Generate global code coverage report
	@go test -cover ${_CURDIR}

.PHONY: coverhtml
coverhtml: ## Generate global code coverage report in HTML
	[ -x /opt/tools/bin/coverage.sh ] && /opt/tools/bin/coverage.sh html || \
	${_CURDIR}/scripts/tools/coverage.sh html ${_CURDIR}/build/coverage.html

.PHONY: dep
dep: ## Get the dependencies
	@go mod vendor

.PHONY: build
build: ## Build the server
	@CGO_ENABLED=0 go build \
	-v -mod=vendor -installsuffix cgo \
	-ldflags '-w -extldflags "-static"' \
	-o ${_CURDIR}/build/pbkdf2 \
	${_CURDIR}/cmd/pbkdf2


.PHONY: build_docker
build_docker: build ## Pack into docker image
	@docker build -t ${_DOCKER_IMG} -f ${_CURDIR}/build/docker/Dockerfile ${_CURDIR}




.PHONY: help
help: ## Display this help screen
	@grep -h -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'
