module errors-example-pbkdf2

go 1.16

require (
	github.com/BurntSushi/toml v0.3.1
	github.com/gofiber/fiber/v2 v2.7.1
	github.com/gofiber/template v1.6.8
	github.com/nicksnyder/go-i18n/v2 v2.1.2
	github.com/segmentio/conf v1.2.0
	github.com/sirupsen/logrus v1.8.1
	gitlab.com/ovsinc/errors v1.1.2
	go.uber.org/fx v1.13.1
	golang.org/x/crypto v0.0.0-20200622213623-75b288015ac9
	golang.org/x/text v0.3.5
	gopkg.in/go-playground/assert.v1 v1.2.1 // indirect
	gopkg.in/yaml.v2 v2.4.0 // indirect
)
