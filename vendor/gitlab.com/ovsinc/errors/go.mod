module gitlab.com/ovsinc/errors

go 1.12

require (
	github.com/BurntSushi/toml v0.3.1
	github.com/coreos/go-systemd v0.0.0-20191104093116-d3cd4ed1dbcf
	github.com/davecgh/go-spew v1.1.1
	github.com/go-stack/stack v1.8.0 // indirect
	github.com/goccy/go-json v0.4.11
	github.com/hashicorp/go-multierror v1.1.0
	github.com/inconshreveable/log15 v0.0.0-20201112154412-8562bdadbbac
	github.com/mattn/go-colorable v0.1.8 // indirect
	github.com/nicksnyder/go-i18n/v2 v2.1.2
	github.com/sirupsen/logrus v1.8.0
	github.com/stretchr/testify v1.4.0
	github.com/valyala/bytebufferpool v1.0.0
	go.uber.org/atomic v1.7.0 // indirect
	go.uber.org/multierr v1.5.0
	go.uber.org/zap v1.16.0
	golang.org/x/text v0.3.5
	golang.org/x/xerrors v0.0.0-20190717185122-a985d3407aa7
)
