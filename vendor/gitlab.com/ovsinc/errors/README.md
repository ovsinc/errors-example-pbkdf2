# Errors

[![pipeline status](https://gitlab.com/ovsinc/errors/badges/master/pipeline.svg)](https://gitlab.com/ovsinc/errors/-/commits/master)
[![coverage report](https://gitlab.com/ovsinc/errors/badges/master/coverage.svg)](https://gitlab.com/ovsinc/errors/-/commits/master)

See [README russian](https://gitlab.com/ovsinc/errors/-/blob/master/README_rus.md).
