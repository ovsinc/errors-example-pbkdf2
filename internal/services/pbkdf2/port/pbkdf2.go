package port

import (
	"encoding/base64"
	"errors-example-pbkdf2/internal/common/messages"
	"errors-example-pbkdf2/internal/services/common"
	"errors-example-pbkdf2/internal/services/pbkdf2/app"
	"errors-example-pbkdf2/internal/services/pbkdf2/domain"
	"strconv"

	"gitlab.com/ovsinc/errors"

	"github.com/gofiber/fiber/v2"
	"github.com/sirupsen/logrus"
)

const EType = "pbkdf2"

var (
	ErrBadIterType = errors.New(
		"bad iter type",
		errors.SetErrorType(EType),
		errors.AppendContextInfo("module", "pbkdf2.port"),
		errors.SetID(messages.IDErrBadIterType),
	)
	ErrBadKeyLenType = errors.New(
		"bad key len type",
		errors.SetErrorType(EType),
		errors.AppendContextInfo("module", "pbkdf2.port"),
		errors.SetID(messages.IDErrBadKeyLenType),
	)
	ErrBadHashType = errors.New(
		"bad hash type",
		errors.SetErrorType(EType),
		errors.AppendContextInfo("module", "pbkdf2.port"),
		errors.SetID(messages.IDErrBadHashType),
	)
	ErrPBKDF2Failed = errors.New(
		"pbkdf2 failed",
		errors.SetErrorType(EType),
		errors.AppendContextInfo("module", "pbkdf2.port"),
		errors.SetID(messages.IDErrPBKDF2Failed),
	)
)

func RegistryKeyRenderService(g fiber.Router, services common.ServicesMap) {
	for _, handler := range services {
		g.Add(handler.Method, handler.Path, handler.Handler)
	}
}

type Pbkdf2Req struct {
	Password, Hash string
	Iter, KeyLen   string
}

type KeyRenderer interface {
	Key(c *fiber.Ctx) error
}

func NewKeyRender(logger *logrus.Logger, kdf app.Pbkdf2er) KeyRenderer {
	return &keyRender{kdf: kdf, logger: logger}
}

type keyRender struct {
	logger *logrus.Logger
	kdf    app.Pbkdf2er
}

func (s *keyRender) Key(c *fiber.Ctx) error {
	localizer := messages.Localizer(
		c.Get("Accept-Language", "en"),
	)

	req := Pbkdf2Req{}

	if err := c.BodyParser(&req); err != nil {
		s.logger.WithError(err).Errorf("data: %#v", req)
		return c.Status(fiber.StatusBadRequest).SendString(err.Error())
	}

	iters, err := strconv.ParseUint(req.Iter, 10, 32)
	if err != nil {
		errors.Log(
			errors.Wrap(
				ErrBadIterType.WithOptions(
					errors.AppendContextInfo("method", "keyRender.Key"),
					errors.AppendContextInfo("kind", "can`t parse iter"),
					errors.AppendContextInfo("data", req.Iter),
				),
				err,
			),
		)

		return c.Status(fiber.StatusBadRequest).SendString(
			ErrBadIterType.
				WithOptions(errors.SetLocalizer(localizer)).
				TranslateMsg(),
		)
	}

	keyLen, err := strconv.ParseUint(req.KeyLen, 10, 32)
	if err != nil {
		errors.Log(
			errors.Wrap(
				ErrBadKeyLenType.WithOptions(
					errors.AppendContextInfo("method", "keyRender.Key"),
					errors.AppendContextInfo("kind", "can`t parse key len"),
					errors.AppendContextInfo("data", req.KeyLen),
				),
				err,
			),
		)

		return c.Status(fiber.StatusBadRequest).SendString(
			ErrBadKeyLenType.
				WithOptions(errors.SetLocalizer(localizer)).
				TranslateMsg(),
		)
	}

	h, err := domain.ParseHashEnum(req.Hash)
	if err != nil {
		errors.Log(
			errors.Wrap(
				ErrBadHashType.WithOptions(
					errors.AppendContextInfo("method", "keyRender.Key"),
					errors.AppendContextInfo("kind", "can`t parse hash"),
					errors.AppendContextInfo("data", req.Hash),
				),
				err,
			),
		)

		return c.Status(fiber.StatusBadRequest).SendString(
			ErrBadHashType.
				WithOptions(errors.SetLocalizer(localizer)).
				TranslateMsg(),
		)
	}

	data, err := s.kdf.Key(&domain.Payload{
		Password: req.Password,
		Iter:     uint32(iters),
		KeyLen:   uint32(keyLen),
		Hash:     h,
	})
	if err != nil {
		errors.Log(
			errors.Wrap(
				ErrPBKDF2Failed.WithOptions(
					errors.AppendContextInfo("method", "keyRender.Key"),
					errors.AppendContextInfo("kind", "call kdf failed"),
					errors.AppendContextInfo("data", req),
				),
				err,
			),
		)

		if hasherr := errors.UnwrapByID(err, messages.IDErrBadHashAlgo); hasherr != nil {
			return c.Status(fiber.StatusInternalServerError).SendString(
				hasherr.WithOptions(
					errors.SetLocalizer(localizer),
				).TranslateMsg(),
			)
		}

		return c.Status(fiber.StatusBadRequest).SendString(
			ErrPBKDF2Failed.
				WithOptions(errors.SetLocalizer(localizer)).
				TranslateMsg(),
		)
	}

	return c.Status(fiber.StatusOK).JSON(
		map[string]string{
			"body": base64.StdEncoding.EncodeToString([]byte(data)),
		},
	)
}
