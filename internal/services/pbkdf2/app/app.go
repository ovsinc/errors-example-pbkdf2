package app

import (
	"errors-example-pbkdf2/internal/services/pbkdf2/domain"
)

func NewGetKeyQuery(pbkdf2 Pbkdf2er) domain.GetKeyQuery {
	return &getKeyQuery{pbkdf2: pbkdf2}
}

type getKeyQuery struct {
	pbkdf2 Pbkdf2er
}

func (a *getKeyQuery) Handle(payload *domain.Payload) ([]byte, error) {
	return a.pbkdf2.Key(payload)
}
