package app

import "errors-example-pbkdf2/internal/services/pbkdf2/domain"

type Pbkdf2er interface {
	Key(payload *domain.Payload) ([]byte, error)
}
