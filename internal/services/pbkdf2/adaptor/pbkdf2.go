package adaptor

import (
	"errors-example-pbkdf2/internal/common/messages"
	"errors-example-pbkdf2/internal/services/pbkdf2/app"
	"errors-example-pbkdf2/internal/services/pbkdf2/domain"

	"gitlab.com/ovsinc/errors"
	"golang.org/x/crypto/pbkdf2"
)

const EAType = "pbkdf2.func"

var ErrBadHashAlgo = errors.New(
	"unusable hash algo",
	errors.SetErrorType(EAType),
	errors.AppendContextInfo("module", "pbkdf2.adaptor"),
	errors.SetID(messages.IDErrBadHashType),
)

func Newkdf() app.Pbkdf2er {
	return &kdf{}
}

type kdf struct{}

func (p *kdf) Key(payload *domain.Payload) ([]byte, error) {
	h, err := payload.Hash.Hash()
	if err != nil {
		return nil, errors.Wrap(
			ErrBadHashAlgo.WithOptions(
				errors.AppendOperations("pbkdf2 function call"),
				errors.AppendContextInfo("kind", "unusable function call"),
				errors.AppendContextInfo("method", "kdf.Key"),
				errors.AppendContextInfo("data", payload),
			),
			err,
		)
	}

	return pbkdf2.Key(
		[]byte(payload.Password),
		[]byte("mysaltismysalt"),
		int(payload.Iter), int(payload.KeyLen),
		h,
	), nil
}
