package domain

type GetKeyQuery interface {
	Handle(payload *Payload) ([]byte, error)
}
