package domain

import (
	"crypto/sha1" //nolint:gosec
	"crypto/sha256"
	"errors"
	"errors-example-pbkdf2/internal/common/hinvalid"
	"hash"
	"strings"
)

type Payload struct {
	Password     string
	Iter, KeyLen uint32
	Hash         HashEnum
}

var (
	ErrUnknownHash = errors.New("unknown hash")
	ErrInvalidHash = errors.New("invalid hash")
)

type HashEnum uint8

func (h HashEnum) Hash() (ret func() hash.Hash, err error) {
	switch h {
	case HashEnum_SHA256:
		ret = sha256.New

	case HashEnum_SHA1:
		ret = sha1.New

	case HashEnum_INVALID:
		return hinvalid.NewShaInvalid, ErrInvalidHash

	default:
		return hinvalid.NewShaInvalid, ErrUnknownHash
	}

	return ret, nil
}

const (
	HashEnum_UNKNOWN HashEnum = iota

	HashEnum_SHA256
	HashEnum_SHA1
	HashEnum_INVALID

	HashEnum_ENDS
)

func ParseHashEnum(s string) (h HashEnum, err error) {
	switch strings.ToUpper(s) {
	case "SHA256":
		h = HashEnum_SHA256

	case "SHA1":
		h = HashEnum_SHA1

	case "INVALID":
		h = HashEnum_INVALID

	default:
		return HashEnum_UNKNOWN, ErrUnknownHash
	}

	return h, nil
}
