package port

import (
	"errors-example-pbkdf2/internal/common/messages"
	"errors-example-pbkdf2/internal/services/common"

	fiber "github.com/gofiber/fiber/v2"
	"github.com/nicksnyder/go-i18n/v2/i18n"
)

func RegistryStaticRenderService(g fiber.Router, services common.ServicesMap) {
	for _, handler := range services {
		g.Add(handler.Method, handler.Path, handler.Handler)
	}
}

//

type StaticRenderer interface {
	Index(c *fiber.Ctx) error
}

func NewStaticRender() StaticRenderer {
	return &staticRender{}
}

type staticRender struct{}

func (s *staticRender) Index(c *fiber.Ctx) error {
	localizer := messages.Localizer(
		c.Get("Accept-Language", "en"),
	)

	return c.Render("index", fiber.Map{
		messages.IDTitle: localizer.MustLocalize(&i18n.LocalizeConfig{
			MessageID:      messages.IDTitle,
			DefaultMessage: messages.StaticMessages[messages.IDTitle],
		}),
		messages.IDPassword: localizer.MustLocalize(&i18n.LocalizeConfig{
			MessageID:      messages.IDPassword,
			DefaultMessage: messages.StaticMessages[messages.IDPassword],
		}),
		messages.IDIterations: localizer.MustLocalize(&i18n.LocalizeConfig{
			MessageID:      messages.IDIterations,
			DefaultMessage: messages.StaticMessages[messages.IDIterations],
		}),
		messages.IDKeylen: localizer.MustLocalize(&i18n.LocalizeConfig{
			MessageID:      messages.IDKeylen,
			DefaultMessage: messages.StaticMessages[messages.IDKeylen],
		}),
		messages.IDHashAlgo: localizer.MustLocalize(&i18n.LocalizeConfig{
			MessageID:      messages.IDHashAlgo,
			DefaultMessage: messages.StaticMessages[messages.IDHashAlgo],
		}),
		messages.IDHashChoose: localizer.MustLocalize(&i18n.LocalizeConfig{
			MessageID:      messages.IDHashChoose,
			DefaultMessage: messages.StaticMessages[messages.IDHashChoose],
		}),
		messages.IDSha1: localizer.MustLocalize(&i18n.LocalizeConfig{
			MessageID:      messages.IDSha1,
			DefaultMessage: messages.StaticMessages[messages.IDSha1],
		}),
		messages.IDSha256: localizer.MustLocalize(&i18n.LocalizeConfig{
			MessageID:      messages.IDSha256,
			DefaultMessage: messages.StaticMessages[messages.IDSha256],
		}),
		messages.IDInvalid: localizer.MustLocalize(&i18n.LocalizeConfig{
			MessageID:      messages.IDInvalid,
			DefaultMessage: messages.StaticMessages[messages.IDInvalid],
		}),
		messages.IDResult: localizer.MustLocalize(&i18n.LocalizeConfig{
			MessageID:      messages.IDResult,
			DefaultMessage: messages.StaticMessages[messages.IDResult],
		}),
		messages.IDDoBtn: localizer.MustLocalize(&i18n.LocalizeConfig{
			MessageID:      messages.IDDoBtn,
			DefaultMessage: messages.StaticMessages[messages.IDDoBtn],
		}),
	})
}
