package common

import "github.com/gofiber/fiber/v2"

type ServiceMap struct {
	Method  string
	Path    string
	Handler func(*fiber.Ctx) error
}

type ServicesMap []ServiceMap
