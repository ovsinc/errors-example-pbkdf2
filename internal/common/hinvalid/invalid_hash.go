package hinvalid

import (
	"hash"
	"io"
)

func NewShaInvalid() hash.Hash {
	return &shaInvalid{}
}

type shaInvalid struct{}

func (i *shaInvalid) Write(p []byte) (n int, err error) {
	return 0, io.ErrUnexpectedEOF
}

func (i *shaInvalid) Size() int            { return 0 }
func (i *shaInvalid) BlockSize() int       { return 0 }
func (i *shaInvalid) Sum(in []byte) []byte { return nil }
func (i *shaInvalid) Reset()               {}
