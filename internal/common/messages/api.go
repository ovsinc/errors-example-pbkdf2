package messages

import "github.com/nicksnyder/go-i18n/v2/i18n"

const (
	IDErrBadIterType   = "ErrBadIterType"
	IDErrBadKeyLenType = "ErrBadKeyLenType"
	IDErrBadHashType   = "ErrBadHashType"
	IDErrBadHashAlgo   = "ErrBadHashAlgo"
	IDErrPBKDF2Failed  = "ErrPBKDF2Failed"
)

var APIMessages = Message{
	IDErrBadIterType: &i18n.Message{
		Other:       "Bad type of iterations.",
		Description: "Ошибка неправильного типа итераций.",
		ID:          IDErrBadIterType,
	},
	IDErrBadKeyLenType: &i18n.Message{
		Other:       "Bad type of key length.",
		Description: "Ошибка неправильного типа длинны ключа.",
		ID:          IDErrBadKeyLenType,
	},
	IDErrBadHashType: &i18n.Message{
		Other:       "Bad type of hash function name.",
		Description: "Ошибка неправильного типа хеша.",
		ID:          IDErrBadHashType,
	},
	IDErrBadHashAlgo: &i18n.Message{
		Other:       "Unusable hash function!!!",
		Description: "Ошибка использования не работающего алгоритма хеширования.",
		ID:          IDErrBadHashAlgo,
	},
	IDErrPBKDF2Failed: &i18n.Message{
		Other:       "PBKDF failed.",
		Description: "Общая ошибка выполнения PKBKF2.",
		ID:          IDErrPBKDF2Failed,
	},
}
