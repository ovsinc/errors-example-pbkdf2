package messages

import (
	"embed"
	"io/fs"

	"github.com/BurntSushi/toml"
	"github.com/nicksnyder/go-i18n/v2/i18n"
	"gitlab.com/ovsinc/errors"
	"golang.org/x/text/language"
)

type Message map[string]*i18n.Message

var (
	//go:embed translations/*
	translationsFS embed.FS

	ErrNoTranslateBundle = errors.New("no i18n bundle")

	bundle     *i18n.Bundle
	localizers = make(map[string]*i18n.Localizer)
)

func init() {
	// files, e := fs.Glob(translationsFS, "active.*.toml")
	files, e := fs.Glob(translationsFS, "translations/active.*.toml")
	if e != nil {
		panic("can`t read translation files")
	}

	if len(files) == 0 {
		panic("no files for translate")
	}

	bundle = i18n.NewBundle(language.English)
	bundle.RegisterUnmarshalFunc("toml", toml.Unmarshal)

	for _, path := range files {
		buf, err := translationsFS.ReadFile(path)
		if err != nil {
			errors.Log(errors.New(
				"can`t read translation file",
				errors.SetErrorType("messages.load"),
				errors.SetOperations("translations.ReadFile"),
				errors.AppendContextInfo("bundle path", path),
				errors.AppendContextInfo("method", "init"),
			))
			continue
		}

		if _, err := bundle.ParseMessageFileBytes(buf, path); err != nil {
			errors.Log(errors.New(
				"can`t parse translation file",
				errors.SetErrorType("messages.load"),
				errors.SetOperations("bundle.ParseMessageFileBytes"),
				errors.AppendContextInfo("bundle path", path),
				errors.AppendContextInfo("method", "init"),
			))
			continue
		}
	}
}

func Localizer(langs ...string) *i18n.Localizer {
	var tag string
	for _, lang := range langs {
		tt, _, err := language.ParseAcceptLanguage(lang)
		if err != nil {
			continue
		}
		for _, t := range tt {
			tag = tag + t.String() + ";"
		}
	}

	loc, ok := localizers[tag]
	if ok {
		return loc
	}

	loc = i18n.NewLocalizer(bundle, langs...)
	localizers[tag] = loc

	return loc
}
