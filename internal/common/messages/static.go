package messages

import "github.com/nicksnyder/go-i18n/v2/i18n"

const (
	IDTitle      = "Title"
	IDPassword   = "Password"
	IDIterations = "Iterations"
	IDKeylen     = "Keylen"
	IDHashAlgo   = "HashAlgo"
	IDHashChoose = "HashChoose"
	IDSha1       = "SHA1"
	IDSha256     = "SHA256"
	IDInvalid    = "INVALID"
	IDDoBtn      = "DoBtn"
	IDResult     = "Result"
)

var StaticMessages = Message{
	IDTitle: &i18n.Message{
		ID:    IDTitle,
		Other: "PBKDF2 function",
	},
	IDPassword: &i18n.Message{
		ID:          IDPassword,
		Description: "Пароль",
		Other:       "Password",
	},
	IDIterations: &i18n.Message{
		ID:          IDIterations,
		Description: "Количество итераций",
		Other:       "Iterations number",
	},
	IDKeylen: &i18n.Message{
		ID:          IDKeylen,
		Description: "Длина ключа",
		Other:       "Key length",
	},
	IDHashAlgo: &i18n.Message{
		ID:          IDHashAlgo,
		Description: "Алгоритм хеширования",
		Other:       "Hash algo",
	},
	IDHashChoose: &i18n.Message{
		ID:    IDHashChoose,
		Other: "Choose...",
	},
	IDSha1: &i18n.Message{
		ID:    IDSha1,
		Other: "SHA1",
	},
	IDSha256: &i18n.Message{
		ID:    IDSha256,
		Other: "SHA256",
	},
	IDInvalid: &i18n.Message{
		ID:          IDInvalid,
		Other:       "INVALID",
		Description: "Ошибочный алго - не используйте!!!",
	},
	IDDoBtn: &i18n.Message{
		ID:    IDDoBtn,
		Other: "Do",
	},
	IDResult: &i18n.Message{
		ID:    IDResult,
		Other: "Result",
	},
}
